//JS

$(document).ready(function () {

//Lazy-Load
var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazyload"
});

//Menu
$(function () {
    $('.hamburger').on('click', function () {
        $('.toggle').toggleClass('open');
        $('.menu').toggleClass('open');
    });
});

//Menu-Scroll
$(window).scroll(function() {
    if($(document).scrollTop() > 0) {
        $('#header').addClass('fixed');
    } else {
        $('#header').removeClass('fixed')
    }
});

//Home-Slider_Owl
$('.home-slider-owl').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//Home-Services_Owl
$('.home-services-owl').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        500:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:3
        },
        1200:{
            items:3
        },
        1400:{
            items:3
        },
        1600:{
            items:3
        }
    }
});

//Home-Clients_Owl
$('.home-clients-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//Home-Projects_Owl
$('.home-projects-owl').owlCarousel({
    loop:true,
    margin:20,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:2
        },
        1200:{
            items:3
        },
        1400:{
            items:3
        },
        1600:{
            items:3
        }
    }
});

//About-Clients_Owl
$('.about-clients-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//About-Team_Owl
$('.about-team-owl').owlCarousel({
    loop:true,
    margin:20,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        },
        1200:{
            items:3
        },
        1400:{
            items:4
        },
        1600:{
            items:4
        }
    }
});

//Services-Clients_Owl
$('.services-clients-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//Project-Detail_Owl
$('.project-detail-owl').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    autoplay:false,
    autoplayTimeout:5000,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//Career-Popup
$(".trigger_open").hide();
$(".close_formen").hide();

$(".trigger").click(function(){		
    $(".trigger_open").toggle();
    $(".containerwe").animate({top: '-10'}, 700);
});

$(".trigger_close").click(function(){	
    $(".trigger_open").hide();
});

$(".close_formen").click(function(){	
    $(".trigger_open").hide();
});

//Faq-Search
$(".faq-search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".accordion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

//Accordion
const items = document.querySelectorAll(".accordion button");

function toggleAccordion() {
  const itemToggle = this.getAttribute('aria-expanded');
  
  for (i = 0; i < items.length; i++) {
    items[i].setAttribute('aria-expanded', 'false');
  }
  
  if (itemToggle == 'false') {
    this.setAttribute('aria-expanded', 'true');
  }
}

items.forEach(item => item.addEventListener('click', toggleAccordion));

});